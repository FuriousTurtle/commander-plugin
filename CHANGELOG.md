# Change Log

All notable changes to the "commander" extension will be documented in this file.

## [3.1.0]

- Fixed issue when coming from an environment with an already configured commander
- Fixed an issue when opening an environment lacking some config files

## [3.0.0]

- The extension will now automatically parse the content of the package.json included in most of the js/ts projects
- You can switch the runner of those package commands in the setting page of the extension webview. The default is npm.
- Remade the webview to make it seamless in the vscode environment, no matter which theme or customization are used.

## [2.0.0]

- Added the webview with live reload to see commands and their descriptions, and be able to start them in current terminal or new terminal

## [1.2.0]

- Added reload of the treeview when creating the commander json file
- Logo change (again)

## [1.1.2]

- README changes

## [1.1.1]

- Logo change

## [1.1.0]

- Added hot reload
- Removed "reload configuration" feature
- Big splits in file architecture for readability and responsibility of classes

## [1.0.0]

- Initial release
- Added everything for the most basic version
- Configuration is set with a default command
- You can run in the current terminal or in a new one
- You can reload the configuration, no vscode restart needed