const vscode = acquireVsCodeApi();

function execInCurrentTerm(cmd) {
    vscode.postMessage({
        command: "exec",
        cmdName: cmd
    })    
}

function execInNewTerm(cmd) {
    vscode.postMessage({
        command: "execInNewTerm",
        cmdName: cmd
    })    
}

function openTab(name) {
    for (let element of document.getElementsByClassName("tabBody")) {
        element.style.display = "none"
    }
    document.getElementById(name).style.display = "block"

    for (let element of document.getElementsByClassName("tabHeader")) {
        element.style.fontWeight = "lighter"
    }
    document.getElementById(`${name}Header`).style.fontWeight = "bold"
}

function applyConf() {
    vscode.postMessage({
        command: "applyConf",
        conf: {
            packageProcessor: document.getElementById("packageProcessor").value
        }
    })
}