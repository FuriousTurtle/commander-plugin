import * as vscode from 'vscode';

export class Command {
	constructor(
		public name: string,
		public command: string,
		public env: {[key: string]: string} = {},
		public description: string = "",
		public withPreprocessor: boolean = false,
		public options: vscode.ShellExecutionOptions = {}){}
}