import * as vscode from 'vscode';
import { Storage } from './modules/storage';
import { Configurator } from './modules/configurator';
import { CommandViewProvider } from './modules/commandViewProvider';

export function activate(context: vscode.ExtensionContext) {

	const storage: Storage = new Storage();

	const provider = new CommandViewProvider(context.extensionUri, storage);
	context.subscriptions.push(vscode.window.registerWebviewViewProvider(CommandViewProvider.viewType, provider));

	Configurator.setupDiskStorage().then(() => {

		context.subscriptions.push(
			vscode.commands.registerCommand('commander.runCommand', () => {
				storage.askForCommand(false);
			}),
			vscode.commands.registerCommand('commander.runCommandNewTerm', () => {
				storage.askForCommand(true);
			})
		);
	});

}

export function deactivate() {}
