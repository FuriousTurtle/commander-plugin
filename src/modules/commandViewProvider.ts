import * as vscode from 'vscode';
import { Command } from '../objects/command';
import { Configurator, CommanderConfig } from "./configurator";
import { Uri } from 'vscode';
import { Storage } from './storage';

export class CommandViewProvider implements vscode.WebviewViewProvider {

	public static readonly viewType = 'commander-explorer-list';

	private _view?: vscode.WebviewView;
	private stylesheetPath?: Uri;
	private codiconsPath?: Uri;
	private scriptPath?: Uri;

	private workspace: string = "Workspace";
	private package: string = "Package";
	private config: string = "Config";

	constructor(
		private readonly _extensionUri: vscode.Uri,
		private readonly storage: Storage
	) {}

	public resolveWebviewView(
		webviewView: vscode.WebviewView,
		_context: vscode.WebviewViewResolveContext,
		_token: vscode.CancellationToken,
	) {
		this._view = webviewView;

		webviewView.webview.options = {
			enableScripts: true,
			localResourceRoots: [
				this._extensionUri
			]
		};

		this.stylesheetPath = this._view.webview.asWebviewUri(vscode.Uri.joinPath(this._extensionUri, 'incSrc', 'stylesheet', 'style.css'));
		this.codiconsPath = this._view.webview.asWebviewUri(vscode.Uri.joinPath(this._extensionUri, 'node_modules', '@vscode/codicons', 'dist', 'codicon.css'));
		this.scriptPath = this._view.webview.asWebviewUri(vscode.Uri.joinPath(this._extensionUri, 'incSrc', 'scripts', 'main.js'));

		Configurator.subscribeToCommandUpdates((conf: CommanderConfig) => {
			webviewView.webview.html = this._getHtmlForWebview(webviewView.webview, conf);
		});
		webviewView.webview.html = this._getHtmlForWebview(webviewView.webview, this.storage.getConf());

		this._view.webview.onDidReceiveMessage(
			message => {
				switch (message.command) {
					case 'exec':
						this.storage.executeCommand(message.cmdName, false);
						return;
					case 'execInNewTerm':
						this.storage.executeCommand(message.cmdName, true);
						return;
					case 'applyConf':
						Configurator.storeConfiguration(message.conf);
				}
			}
		);

	}

	private _getHtmlForWebview(_webview: vscode.Webview, conf: CommanderConfig) {
		return `
			<!DOCTYPE html>
			<html lang="en">
			    <link rel="stylesheet" href="${this.stylesheetPath}" type="text/css" />
				<link href="${this.codiconsPath}" rel="stylesheet" />
				<script src="${this.scriptPath}"></script>
				${this.genHTML(conf)}
			</html>
		`;
	}

	private genHTML(conf: CommanderConfig) {
		let body = "";
		let header = "<div id='tabHeaders'>";

		if (conf.workspaceConfig?.length > 0) {
			header += this.genHeader(this.workspace, "bold");
			body += this.genTab(this.workspace, conf.workspaceConfig, "block");
		} 
		if (conf.packageConfig?.length > 0) {
			header += this.genHeader(this.package);
			body += this.genTab(this.package, conf.packageConfig);
		}

		body += this.genSettingsTab(conf);
		header += `<div style="float: right;" id='${this.config}Header' class='tabHeader' onclick="openTab('${this.config}')"><i class='codicon codicon-settings-gear'></i></div>`;
		header += "</div>";

		return header += body;
	}

	private genTab(name: string, conf: Array<Command>, visibility: string = "none") {
		let tab = `<div id='${name}' class='tabBody' style='display:${visibility}'>`;
		for (const command of conf) {
			tab += (this.genLineForCommand(command));
		}

		return tab += "</div>";
	}

	private genSettingsTab(conf: CommanderConfig) {
		let tab = `
		<div id='${this.config}' class='tabBody' style="display:none">
			<div class="configLine">Package processor: <input id="packageProcessor" value="${conf.packageProcessor || 'npm'}"></div>
			<div class="button apply-button" onclick="applyConf()">Apply</div>
		</div>
		`;

		return tab;
	}

	private genHeader(name: string, fontWeight: string = "lighter") {
		return `
			<div id='${name}Header' class='tabHeader' style='font-weight:${fontWeight}' onclick="openTab('${name}')">${name}</div>
		`;
	}

	private genLineForCommand(cmd: Command) {
		return `
			<div class="grid">
				<div>
					<p class="item-name">${cmd.name}</p>
					<p class="item-description">${cmd.description}</p>
				</div>
				<div class="buttons-container">
					<div class="buttons">
						<div class="button" onclick="execInCurrentTerm('${cmd.name}')"><i class="codicon codicon-terminal"></i></div>
						<div class="button" onclick="execInNewTerm('${cmd.name}')"><i class="codicon codicon-plus"></i></div>
					</div>
				</div>
			</div>
		`;
	}
}