import * as vscode from 'vscode';
import { mkdirSync, writeFileSync, watchFile, Stats} from 'fs';
import defaultConfiguration from '../defaultConfiguration.json';
import { Command } from "../objects/command";
import { CommandParser } from './commandParser';

export type CommanderConfig = {
	"workspaceConfig": Array<Command>,
	"packageConfig": Array<Command>,
	"packageProcessor": String
};

export class Configurator {

	private static commanderConfig: CommanderConfig = {"workspaceConfig": [], "packageConfig": [], "packageProcessor": "npm"};
  	private static fileChangeCallback: Array<(conf: CommanderConfig) => any> = [];
  	private static _onDidChangeTreeData: vscode.EventEmitter<any | undefined | null | void> = new vscode.EventEmitter<any | undefined | null | void>();
  	static readonly onDidChangeTreeData: vscode.Event<any | undefined | null | void> = this._onDidChangeTreeData.event;

	private static refreshTreeView(): void {
		this._onDidChangeTreeData.fire(undefined);
	}

	private static async checkVSCodeFolder(workspace: vscode.WorkspaceFolder): Promise<void> {
		try {
			await vscode.workspace.fs.readDirectory(vscode.Uri.parse(workspace.uri.toString() + "/.vscode"));
		} catch {
			mkdirSync(workspace.uri.toString().substring(7) + "/.vscode");
		}
		return Promise.resolve();
	}

	private static async checkCommanderConfFile(workspace: vscode.WorkspaceFolder): Promise<void> {
		try {
			await vscode.workspace.fs.readFile(vscode.Uri.parse(workspace.uri.toString() + "/.vscode/commander.json"));
		} catch {
			writeFileSync(workspace.uri.toString().substring(7) + "/.vscode/commander.json", JSON.stringify(defaultConfiguration, null, 4));
		}
		return Promise.resolve();
	}

  	private static async getConfiguration(): Promise<CommanderConfig> {
		if (vscode.workspace.workspaceFolders) {
		
			let configData = undefined;
			try {
				configData = await vscode.workspace.fs.readFile(vscode.Uri.parse(vscode.workspace.workspaceFolders[0].uri.toString() + "/.vscode/commander.json")); 
			} catch(e) {console.debug(e);}


			let packageData = undefined;
			try {
				packageData = await vscode.workspace.fs.readFile(vscode.Uri.parse(vscode.workspace.workspaceFolders[0].uri.toString() + "/package.json"));
			} catch(e) {console.debug(e);}

			if (packageData) {Configurator.commanderConfig.packageConfig = CommandParser.parsePackageCommands(JSON.parse(packageData.toString()));}
			if (configData) {
				Configurator.commanderConfig.packageProcessor =	JSON.parse(configData.toString()).packageProcessor;
				Configurator.commanderConfig.workspaceConfig = CommandParser.parseCommands(JSON.parse(configData.toString()).commands);
			}

			return Promise.resolve(Configurator.commanderConfig);
		} else { return Promise.reject("No workspace"); }
	}

	public static async setupDiskStorage(): Promise<void> {
		if (vscode.workspace.workspaceFolders) {
			let workspace = vscode.workspace.workspaceFolders[0];
			await Configurator.checkVSCodeFolder(workspace);
			await Configurator.checkCommanderConfFile(workspace);
			Configurator.refreshTreeView();
			Configurator.getConfiguration().then(config => Configurator.fileChangeCallback.forEach(cb => cb(config)));

			watchFile(workspace.uri.toString().substring(7) + "/.vscode/commander.json", (_curr: Stats, _prev: Stats) => {
				Configurator.getConfiguration().then(config => Configurator.fileChangeCallback.forEach(cb => cb(config)));
			});
		}
	}

  	public static subscribeToCommandUpdates(cb: (conf: CommanderConfig) => undefined) {
    	Configurator.fileChangeCallback.push(cb);
  	}

	public static async storeConfiguration(conf: any) {
		if (vscode.workspace.workspaceFolders) {
			let currentConf = await vscode.workspace.fs.readFile(vscode.Uri.parse(vscode.workspace.workspaceFolders[0].uri.toString() + "/.vscode/commander.json"));
			let parsed = JSON.parse(currentConf.toString());
			for (const [key, value] of Object.entries(conf)) {
				parsed[key] = value;
			}
			writeFileSync(vscode.workspace?.workspaceFolders[0]?.uri.toString().substring(7) + "/.vscode/commander.json", JSON.stringify(parsed, null, 4));
			vscode.commands.executeCommand("workbench.action.reloadWindow");
		}
	}
}