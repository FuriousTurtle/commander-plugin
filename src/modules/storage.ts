import { QuickPick } from "../objects/quickpick";
import { Command } from "../objects/command";
import * as vscode from 'vscode';
import { Configurator, CommanderConfig } from "./configurator";

export class Storage {

	constructor(private conf: CommanderConfig = {"packageConfig": [], "workspaceConfig": [], "packageProcessor": "npm"}, private commands: Array<Command> = []){
		Configurator.subscribeToCommandUpdates((conf: CommanderConfig) => {
			this.conf = conf;
			this.commands = [...conf.packageConfig, ...conf.workspaceConfig];
		});
  	}

	private quickPicksOf(conf: CommanderConfig): Array<QuickPick> {
		let quickpicks: Array<QuickPick> = [];
		[...conf.packageConfig, ...conf.workspaceConfig].forEach((cmd => quickpicks.push(new QuickPick(cmd.name, cmd.description))));
		return quickpicks;
	}

	private parsedEnv(env: {[key: string]: string}): string {
		let envLine: string = "export";
			for (let [variable, value] of Object.entries(env)) {
				envLine += ` ${variable}=${value} `;
			}
		return envLine;
	}
	
	public askForCommand(newTerm: boolean): void {
		if (this.conf) {
			vscode.window.showQuickPick(this.quickPicksOf(this.conf), {placeHolder: "Select the command to run"}).then(selection => {
				if (!selection) {
					return;
				} else {
					this.executeCommand(selection.label, newTerm);
				}
			});
		} else { vscode.window.showInformationMessage("The configuration contains no conf"); }

	}

	public executeCommand(commandName: string, newTerm: boolean): void {
		let command = this.commands.find(cmd => cmd.name === commandName);

		if (command) {
			let term = newTerm ? vscode.window.createTerminal(command.name) : (vscode.window.activeTerminal || vscode.window.createTerminal(command.name));
			term.show();
			if (command.env && Object.keys(command.env).length > 0) {term.sendText(this.parsedEnv(command.env), true);}
			if (command.withPreprocessor) {term.sendText(`${this.conf.packageProcessor || "npm"} run ${command.command}`, true);} else {term.sendText(command.command, true);}
		}
	}

	public getConf() { return this.conf; }

} 