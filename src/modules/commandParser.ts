import { Command } from "../objects/command";
import * as vscode from 'vscode';

export class CommandParser {

	private static parseCommand(name: string, obj: any): Command {
		return new Command(name, obj.command, obj.environment, obj.description, obj.options);
	}

	static parsePackageCommands(conf: vscode.WorkspaceConfiguration): Array<Command> {
		let {has, get, update, inspect, ...cleanedConf} = conf;

		let commands: Array<Command> = [];
		if (cleanedConf && Object.keys(cleanedConf).length > 0) {
			let scriptsConf: {[key: string]: string} = cleanedConf.scripts;
			if (scriptsConf) {
				for (const [key, value] of Object.entries(scriptsConf)) {
					if (value && value.length > 0) { commands.push(new Command(key, key, undefined, value, true)); }
					else { console.error(`Commander: No command value set for key ${key}`); }
				}
			}
		}

		return commands;
	}

	static parseCommands(conf: vscode.WorkspaceConfiguration): Array<Command> {
		let {has, get, update, inspect, ...cleanedConf} = conf;

		let commands: Array<Command> = [];
		if (cleanedConf && Object.keys(cleanedConf).length > 0) {
			for (const [key, value] of Object.entries(cleanedConf)) {
				if (value.command) { commands.push(this.parseCommand(key, value)); }
				else { console.error(`Commander: No command value set for key ${key}`); }
			}
		}

		return commands;
	}
}