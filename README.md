# TERMINAL COMMANDER

Commander is a small plugin that allows you to add, edit, save and remove custom commands in your coding projects.  
The plugin should stay lightweight, fast, efficient and should not include too many features, it will simply focus on project-related command management.

## Features

The main usage of commander is to declare project-scoped terminal commands that can be started in the main terminal.
The extension can also auto-parse package.json commands and allow to run them with a custom runner (npm by default).
You can also tell commander to run your command inside a new terminal.

Here is a complete list of supported features :
- Project-scoped command configuration
- Add, edit and delete commands from your commander
- Run a command in the current active terminal
- Run a command in a new terminal
- Hot reload of the commands when editing the configuration
- Auto-parsing of the package.json commands

## How to use

Simply add your commands in `.vscode/commander.json`, using the sample model provided, and save the file.  
Done ! You'll have your newly updated command list accessible when using commander actions. 

![On-the-fly command management](images/example-1.gif)

You can also access your commands through the commander webview directly.

![Webview available with list of commands](images/example-2.gif)

## Known Issues

List of things to add/improve in no particular order :

- Add support for non-default terminal bin
- Add some coverage with unit tests
- Add translations relative to workspace language
- Add a global command space
- Add the possibility to set and edit commands through a form instead of json config
- Add a shortcut to that form and to the config file through the webview
- Hot reload of package json commands

## Release Notes

### 3.1.0

- Fixed issue when coming from an environment with an already configured commander
- Fixed an issue when opening an environment lacking some config files

### 3.0.0

JSON package commands auto-parsing
- The extension will now automatically parse the content of the package.json included in most of the js/ts projects
- You can switch the runner of those package commands in the setting page of the extension webview. The default is npm.

Webview overhaul
- Remade the webview to make it seamless in the vscode environment, no matter which theme or customization are used.

### 2.0.0

The webview is available
- Added the webview with live reload to see commands and their descriptions, and be able to start them in current terminal or new terminal

### 1.2.0

- Added a reload of the tree view when the json configuration file is created so that it's not confusing to use
- Changed the logo again to make it simpler and easier to understand

### 1.1.2

- Fixed howto in the README

### 1.1.1

- Logo change

### 1.1.0

Hot reload feature is available
- Added the reloading of commands on configuration save
- Removed the "reload configuration" command which is not needed anymore
- Big splits in file architecture for readability and responsibility of classes

### 1.0.0

Initial release of commander
- `Refresh`, `run`, and `run in new terminal` are supported